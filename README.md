# TEV Website

Vet TESS documents for transiting planets.

# Development Installation

The website can be run without a deployment for development and testing. To set this up you must have python 2.7 with headers installed as well as the python virtualenv package.

If you are using Debian/Ubuntu you can install these packages with this command::

    sudo apt install python-dev python-virtualenv

Once you have the required packages, you can setup the default test setup with these commands::

    ./init

To run the web server, you can then run this command and visit the url printed in the output::

    ./manage.py runserver

# Deployment Instalation

To deploy the website, you will need nginx and postgresql installed.

 ... More docs here ...

