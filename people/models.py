#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#


import os
from datetime import datetime

from django.conf import settings
from django.db.models import *
from django.dispatch import receiver
from django.utils.encoding import python_2_unicode_compatible
from django.core.urlresolvers import reverse

from django.contrib.auth.models import Group, AbstractUser

@python_2_unicode_compatible
class User(AbstractUser):
    initials = CharField(max_length=3)

    def __str__(self):
        return self.get_name()

    def get_absolute_url(self):
        return reverse('people:view_profile', kwargs={'username':self.username})

    def get_name(self):
        """Returns the identifable name"""
        return self.first_name or self.last_name or self.username

    def get_initials(self):
        """Generates or gets initials"""
        if self.initials:
            return self.initials
        if self.first_name and self.last_name:
            return (self.first_name[0] + self.last_name[0]).upper()
        return self.username[0].upper()

    def get_role(self):
        """Returns the first group this user is a member of"""
        return self.groups.first()

