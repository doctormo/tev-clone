#
# Copyright 2015, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

from django.views.generic import UpdateView, DetailView
from django.utils.translation import ugettext_lazy as _

from .models import User
from .forms import UserForm
from .mixins import LoginRequiredMixin, NeverCacheMixin, UserMixin, NextUrlMixin

class EditProfile(NeverCacheMixin, NextUrlMixin, UserMixin, UpdateView):
    title = _('Edit User Profile')
    form_class = UserForm

class UserDetail(DetailView):
    slug_url_kwarg = 'username'
    slug_field     = 'username'
    model = User

class MyProfile(NeverCacheMixin, UserMixin, UserDetail):
    pass
  

