#
# Copyright 2015, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

from django.forms import *
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm

from captcha.fields import ReCaptchaField

from .models import User

class PasswordForm(PasswordResetForm):
    recaptcha = ReCaptchaField(label="Human Test")

class UserForm(ModelForm):
    password1 = CharField(label=_('Password'), widget=PasswordInput(), required=False)
    password2 = CharField(label=_('Confirm'), widget=PasswordInput(), required=False)

    class Meta:
        model = User
        exclude = ('user_permissions', 'is_superuser', 'groups', 'last_login',
                   'is_admin', 'is_staff', 'is_active', 'date_joined', 'password')

    def fieldsets(self):
        d = dict((field.name, field) for field in self)
        yield _("Account Settings"), [ d[k]
            for k in ['username', 'password1', 'password2']]
        yield _("Personal Details"), [ d[k]
            for k in ['first_name', 'last_name', 'initials', 'email']]

    def clean(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2:
            if password1 != password2:
                raise ValidationError("Passwords don't match")
            self.cleaned_data['password'] = password1


        return self.cleaned_data

    def clean_username(self):
        username = self.cleaned_data['username']
        user = User.objects.filter(username=username)
        if user and user[0] != self.instance:
            raise ValidationError('Username already taken')
        return username
        
    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        first_name = first_name.strip()
        return first_name
      
    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        last_name = last_name.strip()
        return last_name

    def save(self, **kwargs):
        password = self.cleaned_data.get('password', None)
        if password:
            self.instance.set_password(password)
        ModelForm.save(self, **kwargs)


