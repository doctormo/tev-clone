#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import url, include
from django.views.generic.base import TemplateView
from django.contrib.auth.views import *

from .views import *
from .forms import PasswordForm

def url_tree(regex, *urls):
    return url(regex, include(urls))

UIDB = r'^pwd/(?P<uidb64>[0-9A-Za-z_\-]+?)/(?P<token>.+)/$'


urlpatterns = [
  url(r'^login/',     login, name='login'),
  url(r'^logout/',    logout, name='logout'),

  url(r'^pwd/$',      password_reset, {'password_reset_form': PasswordForm, 'post_reset_redirect': 'people:password_reset_done' }, name='password_reset'),
  url(UIDB,           password_reset_confirm,  name='password_reset_confirm'),
  url(r'^pwd/done/$', password_reset_complete, name='password_reset_complete'),
  url(r'^pwd/sent/$', password_reset_done,     name='password_reset_done'),

  url(r'^$',                     MyProfile.as_view(),   name='my_profile'),
  url(r'^edit/$',                EditProfile.as_view(), name='edit_profile'),
  url(r'^~(?P<username>[^\/]+)', UserDetail.as_view(),  name='view_profile'),
]
