#
# Automatically generated, see django LICENSE
#

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tev.settings")

application = get_wsgi_application()
