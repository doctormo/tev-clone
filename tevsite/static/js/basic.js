
$(document).ready(function() {
  $("[data-toggle=tooltip]").tooltip();
  $(document).ready(function() {
    $(".dropdown-toggle").dropdown();
  });

  $(".modal-dialog .tab").click(modalfieldsets);
  $(".modal-dialog .fields").hide();
  var selected = $(".modal-dialog .tab.selected");
  if(selected.length == 0) {
    selected = $(".modal-dialog .tab:first-child");
  }
  selected.click();
});

function modalfieldsets(event) {
  var target = $(this);
  var previous = $('.tab.selected', target.parent);
  if(target && target != previous) {
    if(previous) {
      previous.removeClass('selected');
      var fields = $('#' + previous.attr('for'));
      fields.hide();
    }   
    target.addClass('selected');
    var fields = $('#' + target.attr('for'));
    fields.show();
  }
}
