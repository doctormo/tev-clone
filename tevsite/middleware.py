#
# Copyright 2015, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#


from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import smart_unicode

from django.conf import settings
from django.db.models import Manager, QuerySet

from .utils import BaseMiddleware, to


class AutoBreadcrumbMiddleware(BaseMiddleware):
    """
    This middleware controls and inserts some breadcrumbs
    into most pages. It attempts to navigate object hierachy
    to find the parent 
    """
    keys = ('breadcrumbs', 'title')

    def process_template_response(self, request, response):
        if not hasattr(response, 'context_data'):
            return response
        data = response.context_data
        for key in self.keys:
            response.context_data[key] = self.get(data, key, then=self)
        return response

    def get_title(self, data):
        """If no title specified in context, use last breadcrumb"""
        if data.get('breadcrumbs', False):
            return list(data['breadcrumbs'])[-1][-1]
        return None

    @to(list)
    def get_breadcrumbs(self, data):
        """Return breadcrumbs only called if no breadcrumbs in context"""
        title = self.get(data, 'title')
        parent = self.get(data, 'parent')
        obj = self.get(data, 'object')
        page = self.get(data, 'current_page')
        if not obj and page:
            # django-cms pages already have Home
            obj = page
        else:
            yield (reverse('index'), _('Home'))

        root = self.get(data, 'breadcrumb_root')
        if root:
            if isinstance(root, list):
                for item in root:
                    yield self.object_link(item)
            else:
                yield self.object_link(root)

        lst = self.get(data, 'object_list')
        if isinstance(lst, (Manager, QuerySet)):
            if obj is None:
                obj = lst
            elif parent is None:
                parent = lst

        for obj in self.get_ancestors(obj, parent):
            link = self.object_link(obj)
            if not (isinstance(link, tuple) and len(link) > 0 and link[0] is not None):
                continue
            if link is not None:
                yield link

        if title is not None:
            yield (None, title)

    def get_ancestors(self, obj, parent=None):
        if hasattr(obj, 'breadcrumb_parent'):
            parent = obj.breadcrumb_parent()
        else:
            parent = getattr(obj, 'parent', parent)

        if parent is not None:
            for ans in self.get_ancestors(parent):
                yield ans
        yield obj

    def object_link(self, obj):
        """Get name from object model"""
        url = None
        if obj is None or (isinstance(obj, tuple) and len(obj) == 2):
            return obj
        if hasattr(obj, 'breadcrumb_name'):
            name = obj.breadcrumb_name()
        elif hasattr(obj, 'name'):
            name = obj.name
        else:
            try:
                name = smart_unicode(obj, errors='ignore')
            except (UnicodeEncodeError, TypeError) as err:
                try:
                    name = unicode(obj)
                except UnicodeEncodeError:
                    name = "Name Error"
        if hasattr(obj, 'get_absolute_url'):
            url = obj.get_absolute_url()
        if name is not None and name.startswith('['):
            return None
        return (url, name)

