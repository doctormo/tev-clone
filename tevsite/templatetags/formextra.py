#
# Copyright 2014, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

from django.template.library import Library

register = Library()

@register.filter("placeholder")
def add_placeholder(form, text=None):
    if text == None:
        raise ValueError("Placeholder requires text content for widget.")
    form.field.widget.attrs.update({ "placeholder": text })
    return form

@register.filter("autofocus")
def add_autofocus(form):
    form.field.widget.attrs.update({ "autofocus": "autofocus" })
    return form

@register.filter("tabindex")
def add_tabindex(form, number):
    form.field.widget.attrs.update({ "tabindex": number })
    return form

@register.filter("formfield")
def add_form_control(form):
    cls = ['form-control']
    if form.errors:
        cls.append("form-control-danger")
    if getattr(form.field.widget, 'input_type', None) != 'radio':
        form.field.widget.attrs.update({"class": ' '.join(cls)})
    return form

