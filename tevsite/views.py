#
# Copyright 2014, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

__all__ = ('Error',)

from django.conf import settings
from django.views.generic import TemplateView, FormView, ListView


class Error(TemplateView):
    @classmethod
    def as_error(cls, status):
        view = cls.as_view(template_name='error/%s.html' % status)
        def _inner(request):
            response = view(request, status=int(status))
            if hasattr(response, 'render'):
                response.render()
            return response
        return _inner

    def post(self, request, **kw):
        return self.get(request, **kw)

    def get(self, request, **kw):
        context = self.get_context_data(**kw)
        if not settings.DEBUG or kw['status'] != 404:
            try:
                path = request.get_full_path()
                ErrorLog.objects.get_or_create(uri=path, **kw)[0].add()
            except:
                pass
        return self.render_to_response(context, **kw)

