#
# Copyright 2014, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin

from .views import *
from vetting.views import DashboardView

urlpatterns = [
    url(r'^$', DashboardView.as_view(), name='index'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^user/', include('people.urls', namespace='people')),
    url(r'^v/', include('vetting.urls', namespace='vet')),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
  + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


for e in ('403','404','500'):
    locals()['handler'+e] = Error.as_error(e)
    urlpatterns.append(url('^error/%s/$' % e, Error.as_error(e)))

