#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Imports data from XML files + PDF files into the website.
"""

import os
import re
import gzip
import logging

from xssd.parse.xmlsd import ParseXML

from django.core.files.base import File
from django.core.management.base import BaseCommand, CommandError

from vetting.models import Sector, Delivery

PATTERN = r'^(?P<prefix>\w+)-(?P<tic>\d+)-(?P<suffix>\d+_dvr).xml(\.gz)?'
MATCHER = re.compile(PATTERN)

def xml_files(name):
    path = os.path.abspath(os.path.expanduser(name))
    if os.path.isfile(path):
        return [path]

    if not os.path.isdir(path):
        raise ArgumentTypeError("File or Path not found: %s" % name)

    return [os.path.join(path, f) for f in os.listdir(path) if MATCHER.search(f)]


class Command(BaseCommand):
    help = 'Imports all the pdf files for vetting'

    def add_arguments(self, parser):
	parser.add_argument('location', nargs='+', type=xml_files)

    def handle(self, location, **kw):
        for bucket in location:
            for filename in bucket:
                items = MATCHER.match(os.path.basename(filename))
                if items:
                    self.add_delivery(filename, **items.groupdict())
                else:
                    logging.warning("Filename '%s' doesn't match." % filename)

    def add_delivery(self, fn, prefix, tic, suffix):
        (delivery, created) = Delivery.objects.get_or_create(slug=prefix)
        if created:
            logging.info("Found new delivery: '%s'" % prefix)

        path = os.path.dirname(fn)
        if fn.endswith('.gz'):
            fn = gzip.GzipFile(fn)
        data = ParseXML(fn).data['dvTargetResults']

        results = data['planetResults']
        if isinstance(results, dict):
            results = [results]

        for pr in results:
            planet_id = int(pr['_planetNumber'])
            sector_id = int(pr['differenceImageResults']['_sector'])
            (sector, _) = Sector.objects.get_or_create(number=sector_id)

            (tr, created) = delivery.transits.get_or_create(
                toi_id="%d_%d" % (planet_id, int(tic)),
                defaults=dict(
                    tic_id=tic,
                    candidate_id=planet_id,
                    sector_id=sector.pk,
                ),
            )

            report_fn = '%s-%s-%02d-%s.pdf' % (prefix, tic, planet_id, suffix)
            summary_fn = report_fn.replace('dvr', 'dvs')
            report_path = os.path.join(path, report_fn)
            summary_path = os.path.join(path, summary_fn)

            if not os.path.isfile(report_path):
                logging.error('Can\'t find report file: %s' % report_fn)
            else:
                with open(report_path, 'r') as fhl:
                    tr.detail.save(report_fn, File(fhl))

            if not os.path.isfile(summary_path):
                logging.error('Can\'t find report file: %s' % summary_fn)
            else:
                with open(summary_path, 'r') as fhl:
                    tr.summary.save(summary_fn, File(fhl))


