#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from django.db.models import *

from django.conf import settings
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse

TOI_DISPOSITION = (
  ('PC', 'Planet Candidate'),
  ('FA', 'False Alarm'),
  ('FP', 'False Positive'),
  ('MP', 'Maybe Planet'),
)

USER_DISPOSITION = (
  ('P', 'Planet'),
  ('EB', 'Eclipsing Binary'),
  ('VS', 'Variable star (only/primary)'),
  ('IN', 'Instrument Noise/Junk'),
  ('O', 'Interesting other/Don\'t know'),
)

GROUP_DISPOSITION = (
  ('P', 'Planet'),
  ('EB', 'Eclipsing Binary'),
  ('VS', 'Variable star (only/primary)'),
  ('IN', 'Instrument Noise/Junk'),
  ('MP', 'Maybe Planet'),
)

DELIVERY_STATUS = (
  ('O', 'Open to Vetting'),
  ('G', 'Group Vetting'),
  ('C', 'Complete'),
)

@python_2_unicode_compatible
class Sector(Model):
    """
    Is a container which lists TOI or Transits of Interest each
    Transit is two pdf files which users and groups can vet.
    """
    number = PositiveIntegerField()
    created = DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Sector #%d" % self.number

    def get_absolute_url(self):
        return reverse('vet:sector', kwargs={'sector_id': self.number})


@python_2_unicode_compatible
class Delivery(Model):
    """
    A delivery is a batch of reports that arrived together. They sometimes
    occupy more than one sector (for example polar stars)
    """
    slug = SlugField(max_length=64, unique=True)
    status = CharField(max_length=2, choices=DELIVERY_STATUS, default='O')
    created = DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name_plural = 'deliveries'

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse('vet:delivery', kwargs={'delivery_id': self.slug})

    def reviewers(self):
        """Return an ordered list of viewers"""
        return get_user_model().objects\
            .filter(reports__report__delivery=self)\
            .order_by('date_joined').distinct()

    def group(self):
        """Return the first group involved"""
        return Group.objects\
            .filter(reports__report__delivery=self)\
            .first()


class ReportQuerySet(QuerySet):
    def get_progress(self):
        total = self.count()

        count = self.filter(disposition__isnull=False).count()
        yield {
          'css': 'success',
          'reviews': 'concluded',
          'reports': count,
          'percent': "%d" % (100 * (float(count) / total)),
        }

        count = self.filter(groups__isnull=False, disposition__isnull=True).count()
        yield {
          'css': 'warning',
          'reviews': 'group',
          'reports': count,
          'percent': "%d" % (100 * (float(count) / total)),
        }

        qs = self.filter(groups__isnull=True, disposition__isnull=True)\
                 .annotate(reviews=Count('individuals'))
        for x in reversed(range(settings.VET_LIMIT)):
            count = qs.filter(reviews=x + 1).count()
            yield {
              'css': 'danger',
              'reviews': "%d reviews" % (x + 1),
              'reports': count,
              'opacity': 0.5 + (0.5 * (float(x) / (settings.VET_LIMIT-1))),
              'percent': "%d" % ((100 - settings.VET_LIMIT) * (float(count) / total) + 1),
            }

@python_2_unicode_compatible
class TransitReport(Model):
    sector = ForeignKey(Sector, related_name='transits')
    delivery = ForeignKey(Delivery, related_name='transits', null=True)
    toi_id = SlugField(max_length=48)
    tic_id = PositiveIntegerField()
    candidate_id = SlugField(max_length=48, null=True, blank=True)

    summary = FileField(upload_to='reports/summary/')
    detail = FileField(upload_to='reports/detail/')

    disposition = CharField(max_length=2, choices=TOI_DISPOSITION, null=True, blank=True)

    objects = ReportQuerySet.as_manager()

    class Meta:
        unique_together = ('sector', 'toi_id')
        permissions = ('is_officer', 'Is Group Vetting Officer'),

    def __str__(self):
        return str(self.tic_id)

    def get_kwargs(self):
        return dict(delivery_id=self.delivery.slug, toi_id=self.toi_id)

    def get_absolute_url(self):
        return reverse('vet:transit', kwargs=self.get_kwargs())

    def get_vetting_url(self):
        return reverse('vet:transit.vet', kwargs=self.get_kwargs())

    def get_review_url(self):
        return reverse('vet:transit.review', kwargs=self.get_kwargs())

@python_2_unicode_compatible
class IndividualVetting(Model):
    report = ForeignKey(TransitReport, related_name='individuals')
    user = ForeignKey(settings.AUTH_USER_MODEL, related_name='reports')
    disposition = CharField(max_length=2, choices=USER_DISPOSITION)

    comment = TextField(null=True, blank=True)

    created = DateTimeField(auto_now_add=True)
    edited = DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('report', 'user')

    def __str__(self):
        return self.disposition


@python_2_unicode_compatible
class GroupVetting(Model):
    report = ForeignKey(TransitReport, related_name='groups')
    group = ForeignKey('auth.Group', related_name='reports')
    disposition = CharField(max_length=2, choices=GROUP_DISPOSITION)

    comment = TextField(null=True, blank=True)

    created = DateTimeField(auto_now_add=True)
    edited = DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('report', 'group')

    def __str__(self):
        return self.disposition





