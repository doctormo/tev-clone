from django.template import Library
from django.conf import settings
from django.db.models import Count

register = Library()

@register.filter("next_indervidual_report")
def next_indervidual_report(obj, user):
    return obj.transits\
	       .exclude(individuals__user_id=user.pk)\
	       .annotate(count=Count('individuals'))\
	       .exclude(count__gt=settings.VET_LIMIT)\
	       .exclude(disposition__isnull=False)\
	       .order_by('-count', '?').first()

@register.filter("next_group_report")
def next_group_report(obj, group):
    if group:
        return obj.transits\
	       .exclude(groups__group_id=group.pk)\
	       .exclude(disposition__isnull=False)\
	       .order_by('pk').first()

@register.filter("progress")
def progress(obj, user):
    qs = obj.transits.all()
    todo = qs.count()
    done = qs.filter(individuals__user_id=user.pk).count()
    return {
        'count': done,
        'total': todo,
        'percent': "%.2f" % ((float(done) / todo) * 100)
    }

@register.filter("vetting_for")
def vetting_for(obj, user):
    return obj.individuals.filter(user=user).first()

@register.filter("vetting_group")
def vetting_group(obj, group):
    return obj.groups.filter(group=group).first()

