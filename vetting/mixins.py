#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from people.mixins import LoginRequiredMixin

from .models import Sector, Delivery, TransitReport

class SectorMixin(LoginRequiredMixin):
    model = Sector
    slug_url_kwarg = 'sector_id'
    slug_field = 'number'

class DeliveryMixin(LoginRequiredMixin):
    model = Delivery
    slug_url_kwarg = 'delivery_id'

class TransitMixin(LoginRequiredMixin):
    model = TransitReport
    slug_url_kwarg = 'toi_id'
    slug_field = 'toi_id'


