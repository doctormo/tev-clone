#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from django.contrib.admin import *

from .models import *

site.register(Sector)
site.register(Delivery)
site.register(TransitReport)

class VettingAdmin(ModelAdmin):
    list_display = ('report', 'user', 'disposition', 'sector', 'delivery')

    def sector(self, obj):
        return obj.report.sector.number

    def delivery(self, obj):
        return obj.report.delivery.slug


site.register(IndividualVetting, VettingAdmin)
site.register(GroupVetting)

