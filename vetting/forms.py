#
# Copyright 2015, Martin Owens
#           2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#
# Code imported from inkscape-web 2017-10-18 AGPLv3
#

from django.forms import *
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import PasswordResetForm

from captcha.fields import ReCaptchaField

from .models import IndividualVetting, GroupVetting

class BsRadioSelect(RadioSelect):
    template_name = 'bootstrap_radio.html'


class UserVetterForm(ModelForm):
    class Meta:
        model = IndividualVetting
        fields = ('disposition', 'comment')
        widgets = {'disposition': BsRadioSelect}


class GroupVetterForm(ModelForm):
    class Meta:
        model = GroupVetting
        fields = ('disposition', 'comment')
        widgets = {'disposition': BsRadioSelect}


