#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from django.views.generic import ListView, DetailView, FormView, RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.utils.translation import ugettext_lazy as _
from django.contrib import messages

from people.mixins import LoginRequiredMixin
from .templatetags.vetters import next_indervidual_report, next_group_report
from .forms import UserVetterForm, GroupVetterForm
from .models import Sector, Delivery, TransitReport
from .mixins import SectorMixin, DeliveryMixin, TransitMixin


class DashboardView(DeliveryMixin, ListView):
    title = _('Dashboard')

class SectorDetail(SectorMixin, DetailView):
    permission = 'vetting.is_officer'

class DeliveryDetail(TransitMixin, ListView):
    permission = 'vetting.is_officer'
    paginate_by = 20

    def get_object(self):
        return Delivery.objects.get(slug=self.kwargs['delivery_id'])

    def get_queryset(self):
        qs = super(DeliveryDetail, self).get_queryset()
        qs.filter(delivery=self.get_object()).order_by('tic')
        return qs

    def get_context_data(self, **kw):
        data = super(DeliveryDetail, self).get_context_data(**kw)
        data['object'] = self.get_object()
        return data

    def get_title(self):
        return "Transits of Interest (TOI)"

class TransitDetail(TransitMixin, DetailView):
    pass

class DeliveryStatus(DeliveryMixin, SingleObjectMixin, RedirectView):
    def get_redirect_url(self, status, delivery_id):
        delivery = self.get_object()
        delivery.status = status.upper()
        delivery.save()
        messages.success(self.request, '%s set to %s' % (
            unicode(delivery), delivery.get_status_display()))
        if status == 'G':
            item = next_group_report(delivery, self.request.user.get_role())
            return item.get_review_url()
        if status == 'O':
            return '/'
        return delivery.get_absolute_url()

class CreateDisposition(TransitMixin, SingleObjectMixin, FormView):
    template_name = 'vetting/disposition_form.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CreateDisposition, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CreateDisposition, self).post(request, *args, **kwargs)

    def get_form_kwargs(self):
        """Replace instance with Vetting instance instead of TransitReport"""
        qs = self.form_class._meta.model.objects.filter(report=self.object)
        kwargs = super(CreateDisposition, self).get_form_kwargs()
        kwargs['instance'] = qs.filter(**dict([self.get_owner()])).first()
        return kwargs

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.report = self.object
        setattr(obj, *self.get_owner())
        obj.save()
        return super(CreateDisposition, self).form_valid(form)

    def get_context_data(self, **kw):
        data = super(CreateDisposition, self).get_context_data(**kw)
        data['owner'] = dict([self.get_owner()])
        return data


class UserVetterCreate(CreateDisposition):
    title = _('User Vetting')
    form_class = UserVetterForm

    def get_owner(self):
        return ('user', self.request.user)

    def get_success_url(self):
        nxt = next_indervidual_report(self.object.delivery, self.request.user)
        if nxt:
            return nxt.get_vetting_url()
        return '/'

class GroupVetterCreate(CreateDisposition):
    title = _('Group Vetting')
    form_class = GroupVetterForm
    permission = 'vetting.is_officer'

    def get_owner(self):
        return ('group', self.request.user.get_role())

    def get_success_url(self):
        nxt = next_group_report(self.object.delivery, self.request.user.get_role())
        if nxt:
            return nxt.get_review_url()
        return '/'

