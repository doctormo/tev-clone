#
# Copyright 2017, MIT TESS
#
# tess-tev is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# tess-tev is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with tess-tev.  If not, see <http://www.gnu.org/licenses/>.
#

from django.conf.urls import url, include

from .views import (
    SectorDetail, DeliveryDetail, TransitDetail,
    UserVetterCreate, GroupVetterCreate, DeliveryStatus,
)

urlpatterns = [
  url(r'^(?P<sector_id>\d+)/$', SectorDetail.as_view(), name='sector'),

  url(r'^(?P<delivery_id>[\w\-\_]+)/$',                        DeliveryDetail.as_view(), name='delivery'),
  url(r'^(?P<delivery_id>[\w\-\_]+)/status/(?P<status>[OGC])/$', DeliveryStatus.as_view(), name='delivery.status'),
  url(r'^(?P<delivery_id>[\w\-\_]+)/(?P<toi_id>\w+)/$',        TransitDetail.as_view(), name='transit'),
  url(r'^(?P<delivery_id>[\w\-\_]+)/(?P<toi_id>\w+)/vet/$',    UserVetterCreate.as_view(), name='transit.vet'),
  url(r'^(?P<delivery_id>[\w\-\_]+)/(?P<toi_id>\w+)/review/$', GroupVetterCreate.as_view(), name='transit.review'),
]
